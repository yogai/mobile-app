import React from 'react';
import { StyleSheet, Text, Button, View } from 'react-native';

export default class App extends React.Component {
  _continueWithTime() {
    console.log(time)
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>How many time do you want to practive ?</Text>

        <Button
          containerViewStyle={{width: '100%', marginLeft: 0}}
          onPress={this._continueWithTime}
          title="Custom"
          // color="#841584"
          accessibilityLabel="Make 8 min activity"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
});
